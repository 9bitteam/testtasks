﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using Newtonsoft.Json;
namespace WebApplication1.Controllers
{
  public class HomeController : Controller
  {
    // пример использования
    // хост:порт/?insert={"id":"cfaa0d3f-7fea-4423-9f69-ebff826e2f89","operationDate":"2019-04-02T13:10:20.0263632+03:00","amount":23.05}
    // хост:порт/?get=cfaa0d3f-7fea-4423-9f69-ebff826e2f89

    [HttpGet]
    public string Index(string insert = null, string get = null)
    {
      // если параметры не пришли, возвращаем соответствующий JSON
      if (get == null && insert == null)
      {
        return "{\"Warning\": \"Empty request\"}";
      }
      if (insert != null)
      {
        try
        {
          // костыль, все остальные решения влияют на состав ТЗ в рамках asp.net core
          // в ASP.CORE нет непосредственного управления IIS, поэтому без танцов с бубнами нельзя заставить его пропускать символ +, 
          // который существует в стандартном представлении времени в JSON данных, по умолчанию он замещается пробелом
          // для более корректной обработки запросов потребуется допустить иной протокол взаимодействия 
          insert = insert.Replace(" ", "+");

          Entity entity = JsonConvert.DeserializeObject<Entity>(insert);
          // если запись с таким Guid уже есть, пропускаем
          
          
          lock (Program.entitiesLock)
            if (Program.entities.Where(e => e.Id.CompareTo(entity.Id) == 0).Count() > 0)
              return "{\"Result\": \"Fail\"}, \"Error\": \"Already exists\"";
          try
          {
            // защита от единомоментного доступа
            lock (Program.entitiesLock)
            { 
              //добавление в хранилище 
              Program.entities.Add(entity);
            }
          }
          catch (OutOfMemoryException ex1)
          {
            // отдельно вылавливается наиболее вероятная ошибка
            return "{\"Result\": \"Fail\"}, \"Error\": \"Out of Memory\"";
          }
          catch (Exception ex2)
          {
            // общий вылов ошибок
            return "{\"Result\": \"Fail\"}, \"Error\": \"Error while save to memory: " + ex2.Message + "\"";
          }
          // сообщение об успешной операции
          return "{\"Result\": \"Success\"}";
        }
        catch (Exception e)
        {
          // вывод ошибки которая может возникнуть при десериализации или добавлении в базу
          return "{\"Result\": \"Fail\", \"Error\": \"Error while Deserialise: " + e.Message + " \"}";
        }
      }
      else if (get != null)
      {
        
        Entity entity = null;

        try
        {
          // ищем наличие Entity с таким id в базе. Guid.CompareTo - наиболее быстрая проверка
          // на время поиска защищаем коллецию от паралельного доступа
          Guid search_id = new Guid(get);
          lock (Program.entitiesLock)
            entity = Program.entities.Where(e => e.Id.CompareTo(search_id) == 0).FirstOrDefault();

          // если объект нашелся - ...
          if (entity != null)
            return JsonConvert.SerializeObject(entity);
          else
            return "{\"Result\": \"Fail\", \"Error\": \"No object with same id in DB \"}";

        }
        catch (Exception e)
        {
          // обработка ошибок, может быть кривой id или проблемы с коллекцией
          return "{\"Result\": \"Fail\", \"Error\": \"Error while search object: " + e.Message + " \"}";
        }
      }

      // пыщ пыщ ололо
      return "Unknown State";
    }

    public IActionResult PostRequestHelper()
    {
      return View();
    }
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]

    // 
    public IActionResult Error()
    {
      return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }

  }
}
