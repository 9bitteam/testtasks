﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
  class Program
  {
    /// <summary>
    /// Отвечсает за работу основного цикла программы, после установки в false цикл будет завершен
    /// </summary>
    private static bool doDialog = true;
    static void Main(string[] args)
    {
      DataAdapter dataAdapter = new DataAdapter();

      // цикл захвата действий из UI
      while (doDialog)
      {
        // вывод основного меню
        switch (DialogHelper.DrawMainMenu())
        {
          case 1:
            // вводимые юзером данные
            string id = "", date = "", amount = "";
            // вывод диалога добавления транзакции
            DialogHelper.ShowAddTransactionDialog(ref id, ref date, ref amount);
            // иницирование результата и попытка создания транзакции 
            CreateTransactionState state = CreateTransactionState.Success;
            Transaction t = Transaction.Parse(id, date, amount, ref state);
            // обработка ошибки парсинга
            if (t == null)
            {
              DialogHelper.ShowParseTransactionErrorMessage(state);
              continue;
            }
            // попытка добавления транзакции 
            var result = dataAdapter.AddTransaction(t);
            // вывод сообщения с результатом
            DialogHelper.ShowAddTransactionResultMessage(result);

            break;
          case 2:
            // получаем вводимые юзером данные для поиска
            string search_id = "";
            DialogHelper.ShowGetTransactionDialog(ref search_id);
            GetTransactionResult search_state = GetTransactionResult.Found;
            Transaction transaction = dataAdapter.GetTransaction(search_id, ref search_state);
            if (transaction == null)
            {
              DialogHelper.ShowGetTrunsactionResultMessage(search_state);
              continue;
            }
            else
            {
              DialogHelper.ShowTransactionData(transaction);
            }
            break;
          default:
            doDialog = false;
            break;

        }
      }


      Console.WriteLine("Работа программы завершена \npress any key to continue...");
      Console.ReadKey();
      Console.WriteLine("ololo ;)");
    }
  }
}
