﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
  class DataAdapter
  {
    private bool initialized = false;
    private Dictionary<int, Transaction> Transactions;
    /// <summary>
    /// Получает статус инициализации адаптера данных
    /// </summary>
    public bool Initialized { get { return initialized; } }

    public DataAdapter()
    {
      this.Init();
    }

    /// <summary>
    /// Инициализирует хранилище данных в оперативной памяти, если ранее не было иницилизировано
    /// </summary>
    public void Init()
    {
      // если словарь транзакций пуст, создаем новый экземпляр
      if (Transactions == null) Transactions = new Dictionary<int, Transaction>();
      // в любом случае обозначаем хранилиже инициализированным
      initialized = true;
    }

    /// <summary>
    /// Добавляет новый объект Transaction в коллекцию, если элемент с таким ID уже существует, его данные будут обновлены
    /// </summary>
    /// <param name="transaction">добавляемый элемент</param>
    public AddTrasactionResult AddTransaction(Transaction transaction)
    {
      try
      {
        if (Transactions.ContainsKey(transaction.Id))
        {
          Transactions[transaction.Id] = transaction;
          return AddTrasactionResult.SuccessButReplaced;
        }
        else
        {
          Transactions.Add(transaction.Id, transaction);
          return AddTrasactionResult.Success;
        }
      }
      catch (OutOfMemoryException ex1)
      {
        return AddTrasactionResult.UnsuccessOutOfMemory;
      }
      catch (Exception ex2)
      {
        return AddTrasactionResult.UnsuccessUnknownCase;
      }
    }

    /// <summary>
    /// Получает транзакцию из хранилища по id, может вернуть Null если транзакции с таким id нет или при ошибке конвертирования id
    /// </summary>
    /// <param name="id">идентификатор искомой транзакции</param>
    /// <param name="state">результат попытки поиска</param>
    /// <returns></returns>
    public Transaction GetTransaction(string id, ref GetTransactionResult state)
    {

      int Id;

      if (!int.TryParse(id, out Id))
      {
        state = GetTransactionResult.WrongIdInput;
        return null;
      }
      try
      {
        if(Transactions.ContainsKey(Id))
        {
          state = GetTransactionResult.Found;
          return Transactions[Id];
        }
        else
        {
          state = GetTransactionResult.NotFount;
          return null;
        }
      }
      catch (Exception e)
      {
        state = GetTransactionResult.UnknownError;
      }
      return null;
    }
  }

  #region сопутствующие перечисления
  public enum AddTrasactionResult
  {
    Success,
    SuccessButReplaced,
    UnsuccessOutOfMemory,
    UnsuccessUnknownCase
  }
  public enum GetTransactionResult
  {
    Found,
    NotFount,
    WrongIdInput,
    UnknownError
  }
  #endregion

}
