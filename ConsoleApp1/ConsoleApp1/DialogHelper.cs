﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
  class DialogHelper
  {

    /// <summary>
    /// Выводит главное меню взаимодействия с коллекцией данных
    /// </summary>
    /// <returns> 1 для операции добавить, 2 для операции для поиска по ID, -1 для другой пользовательской команды</returns>
    public static int DrawMainMenu()
    {
      Console.WriteLine("Введите Add для добавления новой транзакции");
      Console.WriteLine("Введите Get для получения транзакции по ID");
      Console.WriteLine("Введите exit или любой другой симовол для выхода из программы");

      string input = Console.ReadLine().ToLower();
      if (input == "add") return 1;
      else if (input == "get") return 2;
      else return -1;
    }

    /// <summary>
    /// Выводит диалог добавления новой записи
    /// </summary>
    /// <param name="id">заполняется вводом для id</param>
    /// <param name="dateTime"> ...</param>
    /// <param name="amount">... и тд</param>
    public static void ShowAddTransactionDialog(ref string id, ref string dateTime, ref string amount)
    {
      Console.Write("Enter id: ");
      id = Console.ReadLine();

      Console.Write("Enter date: ");
      dateTime = Console.ReadLine();

      Console.Write("Enter amount: ");
      amount = Console.ReadLine();
    }

    /// <summary>
    /// Выводит диалог поиска записи по id
    /// </summary>
    /// <param name="id">заполняется пользовательским вводом идентификатора</param>
    public static void ShowGetTransactionDialog(ref string id)
    {
      Console.Write("Enter id: ");
      id = Console.ReadLine();
    }
    
    /// <summary>
    /// Выводит сообщение с ошибкой парсинга ввода транзакции
    /// </summary>
    /// <param name="state">статус результата попытки парсинга</param>
    public static void ShowParseTransactionErrorMessage(CreateTransactionState state)
    {
      switch(state)
      {
        case CreateTransactionState.Success:
          Console.WriteLine("успешно");
          break;
        case CreateTransactionState.UnsaccessParseDateTime:
          Console.WriteLine("некорректно введена дата-время");
          break;
        case CreateTransactionState.UnsaccessParseId:
          Console.WriteLine("некорректно введен id ");
          break;
        case CreateTransactionState.UnsuccessParseAmount:
          Console.WriteLine("некорректно введена сумма транзакции");
          break;
        case CreateTransactionState.UnsuccessUnknownCase:
          Console.WriteLine("неизвестная ошибка");
          break;
      }
    }
    
    /// <summary>
    /// Выводит сообщение с результатом сохранения транзакции в хранилище
    /// </summary>
    /// <param name="state">Статус попытки сохранения</param>
    public static void ShowAddTransactionResultMessage(AddTrasactionResult state)
    {
      switch(state)
      {
        case AddTrasactionResult.Success:
          Console.WriteLine("OK[]");
          break;
        case AddTrasactionResult.SuccessButReplaced:
          Console.WriteLine("Запись с таким id уже есть, данные транзакции обновлены");
          break;
        case AddTrasactionResult.UnsuccessOutOfMemory:
          Console.WriteLine("Ошибка сохранения транзакции, недостаточно памяти");
          break;
        case AddTrasactionResult.UnsuccessUnknownCase:
          Console.WriteLine("Ошибка сохранения транзакции, неизвестная ошибка");
          break;
      }
    }
    public static void ShowGetTrunsactionResultMessage(GetTransactionResult state)
    {
      switch(state)
      {
        case GetTransactionResult.Found:
          Console.WriteLine("транзакция найдена");
          break;
        case GetTransactionResult.NotFount:
          Console.WriteLine("тразакции с таким id в хранилище нет");
          break;
        case GetTransactionResult.WrongIdInput:
          Console.WriteLine("некорректно введен id");
          break;
        case GetTransactionResult.UnknownError:
          Console.WriteLine("произошла неизвестная ошибка");
          break;


      }
    }
    public static void ShowTransactionData(Transaction transaction)
    {
      Console.WriteLine(transaction.ToJsonString());
      Console.WriteLine("[OK]");
    }
  }
}
