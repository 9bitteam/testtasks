﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ConsoleApp1
{
  public class Transaction
  {
    public int Id { get; set; }
    public DateTime TransactionDate { get; set; }
    public decimal Amount { get; set; }
   
    /// <summary>
    /// список достпуных для парсинга форматов ввода даты-времени
    /// </summary>
    private static string[] DateTimeAvalibleformats = 
    {
      "dd.MM.yyyy hh:mm:ss", "yyyy.MM.dd hh:mm:ss",
      "dd.MM.yyyy",           "yyyy.MM.dd"
    };
    
    /// <summary>
    /// Создает новый экземпляр типа Transaction, при возникновении ошибок изменяет state
    /// </summary>
    /// <param name="id">идентификаатор транзакции</param>
    /// <param name="transactionDate">датавремя транзакции</param>
    /// <param name="amount">сумма</param>
    /// <param name="state">заполняется причиной проблем при </param>
    /// <returns>Созданный объект транзакции или null в случае ошибки</returns>
    public static Transaction Parse(string id, string transactionDate, string amount, ref CreateTransactionState state)
    {
      int Id;
      decimal Amount;
      DateTime TransactionDate;
      // Парсинг id и обработка ошибок 
      if (!System.Int32.TryParse(id, out Id))
      {
        state = CreateTransactionState.UnsaccessParseId;
        return null;
      }

      // Парсинг Amount и обработка ошибок 
      if (!System.Decimal.TryParse(amount, out Amount))
      {
        state = CreateTransactionState.UnsuccessParseAmount;
        return null;
      }

      // Парсинг TransactionDate и обработка ошибок 
      if (!System.DateTime.TryParseExact(transactionDate, DateTimeAvalibleformats, 
        System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, 
        out TransactionDate))
      {
        state = CreateTransactionState.UnsaccessParseDateTime;
        return null;
      }
      // создаем и возвращаем объект
      return new Transaction()
      {
        Id = Id,
        TransactionDate = TransactionDate,
        Amount = Amount
      };

    }

    public string ToJsonString()
    {
      return JsonConvert.SerializeObject(this);
    }
  }

  /// <summary>
  /// перечесление возможных результатов парсинга данных в текстовом формате 
  /// </summary>
  public enum CreateTransactionState
  {
    Success,
    UnsuccessUnknownCase, 
    UnsaccessParseId,
    UnsaccessParseDateTime,
    UnsuccessParseAmount
  }

}
